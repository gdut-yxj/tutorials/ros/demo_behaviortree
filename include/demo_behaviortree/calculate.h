/*******************************************************************************
 * Copyright (c) 2022/09/28, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/
#pragma once
#include <behaviortree_cpp_v3/action_node.h>
#include <ros/ros.h>

#include "demo_behaviortree/AddTwoInts.h"
namespace demo_behaviortree {

class Calculate : public BT::SyncActionNode {
 public:
  Calculate(const std::string& name, const BT::NodeConfiguration& config, const ros::NodeHandle& root_nh,
            const ros::NodeHandle& tree_nh)
      : BT::SyncActionNode(name, config) {
    ros::NodeHandle nh(root_nh);
    ati_client_ = nh.serviceClient<demo_behaviortree::AddTwoInts>("add_two_ints");
    ros::NodeHandle calculate_nh(tree_nh, "calculate");
    a_ = calculate_nh.param("a", 0);
    b_ = calculate_nh.param("b", 0);
  };

  // 这个必须要写，指定接口的参数名称 message
  static BT::PortsList providedPorts() {
    BT::PortsList ports_list;
    return ports_list;
  }

  BT::NodeStatus tick() override {
    demo_behaviortree::AddTwoInts srv;
    srv.request.a = a_;
    srv.request.b = b_;
    if (ati_client_.call(srv)) {
      ROS_INFO("Sum: %ld", (long int)srv.response.sum);
    } else {
      ROS_ERROR("Failed to call service add_two_ints");
      return BT::NodeStatus::FAILURE;
    }
    return BT::NodeStatus::SUCCESS;
  };

 private:
  ros::ServiceClient ati_client_;
  int a_, b_;
};

}  // namespace demo_behaviortree
