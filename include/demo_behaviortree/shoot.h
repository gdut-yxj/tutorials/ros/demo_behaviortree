/*******************************************************************************
 * Copyright (c) 2022/09/28, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/
#pragma once
#include <actionlib/client/simple_action_client.h>
#include <ros/ros.h>

#include "demo_behaviortree/ShootAction.h"
namespace demo_behaviortree {

class Shoot : public BT::SyncActionNode {
 public:
  Shoot(const std::string& name, const BT::NodeConfiguration& config, const ros::NodeHandle& root_nh,
        const ros::NodeHandle& tree_nh)
      : BT::SyncActionNode(name, config) {
    ros::NodeHandle nh(root_nh);
    shoot_client_ = std::make_unique<actionlib::SimpleActionClient<demo_behaviortree::ShootAction>>("shoot", true);
    feedback_sub_ = nh.subscribe<demo_behaviortree::ShootActionFeedback>("shoot/feedback", 1, &Shoot::feedbackCB, this);
  };

  // 这个必须要写，处理接口参数的
  static BT::PortsList providedPorts() {
    BT::PortsList ports_list;
    return ports_list;
  }

  BT::NodeStatus tick() override {
    // 这时是一个简单的不断设计的demo，一次action完毕直接发送新的设计请求
    // 实际上的真正的行为树这里会判断状态，根据具体情况发送cancel 取消action，或者新的action goal
    if (actionlib::SimpleClientGoalState::ACTIVE == shoot_client_->getState().state_) {
      ROS_INFO("Shoot feedback which_shot: %d", fb_.feedback.which_shot);
    } else {
      int num_of_shoot = rand() % 10;  // 随便打几枪
      demo_behaviortree::ShootGoal shoot_goal;
      shoot_goal.number_of_shots = num_of_shoot;
      shoot_client_->sendGoal(shoot_goal);
      ROS_INFO("Send a new goal, shoot_goal.num_of_shoot is %d in this time", num_of_shoot);
    }
    return BT::NodeStatus::SUCCESS;
  };

 private:
  void feedbackCB(const demo_behaviortree::ShootActionFeedback::ConstPtr& msg) { fb_ = *msg; };
  std::unique_ptr<actionlib::SimpleActionClient<demo_behaviortree::ShootAction>> shoot_client_;
  demo_behaviortree::ShootActionFeedback fb_;
  ros::Subscriber feedback_sub_;
};

}  // namespace demo_behaviortree
