/*******************************************************************************
 * Copyright (c) 2022/09/28, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/
#pragma once
#include <behaviortree_cpp_v3/action_node.h>
#include <ros/ros.h>

namespace demo_behaviortree {

class SelfIntro : public BT::SyncActionNode {
 public:
  SelfIntro(const std::string& name, const BT::NodeConfiguration& config) : BT::SyncActionNode(name, config){};

  // 这个必须要写，处理接口参数的
  static BT::PortsList providedPorts() {
    BT::PortsList ports_list;
    ports_list.insert(BT::InputPort<std::string>("my_name"));
    ports_list.insert(BT::InputPort<int>("age"));
    ports_list.insert(BT::OutputPort<std::string>("output"));
    return ports_list;
  }

  BT::NodeStatus tick() override {
    auto my_name = getInput<std::string>("my_name");
    auto age = getInput<int>("age");

    if (!my_name) {
      throw BT::RuntimeError("missing required input [my_name]: ", my_name.error());
    }
    if (!age) {
      throw BT::RuntimeError("missing required input [age]: ", age.error());
    }

    ROS_INFO("my name is %s, I`m %d years old. ", my_name->c_str(), age.value());

    std::string op = "This is the msg of " + std::string(my_name.value());
    setOutput("output", op);
    return BT::NodeStatus::SUCCESS;
  };
};

}  // namespace demo_behaviortree
