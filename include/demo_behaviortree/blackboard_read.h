/*******************************************************************************
 * Copyright (c) 2022/09/28, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/
#pragma once
#include <behaviortree_cpp_v3/action_node.h>
#include <ros/ros.h>
namespace demo_behaviortree {

class BlackboardRead : public BT::SyncActionNode {
 public:
  BlackboardRead(const std::string& name, const BT::NodeConfiguration& config) : BT::SyncActionNode(name, config){};

  // 这个必须要写，处理接口参数的
  static BT::PortsList providedPorts() {
    BT::PortsList ports_list;
    ports_list.insert(BT::InputPort<std::string>("aa_msg"));
    ports_list.insert(BT::InputPort<std::string>("bb_msg"));
    ports_list.insert(BT::InputPort<std::string>("cc_msg"));
    return ports_list;
  }

  BT::NodeStatus tick() override {
    auto aa_msg = getInput<std::string>("aa_msg");
    auto bb_msg = getInput<std::string>("bb_msg");
    auto cc_msg = getInput<std::string>("cc_msg");

    ROS_INFO("aa msg: %s.", aa_msg->c_str());
    ROS_INFO("bb msg: %s.", bb_msg->c_str());
    ROS_INFO("cc msg: %s.", cc_msg->c_str());

    return BT::NodeStatus::SUCCESS;
  };
};

}  // namespace demo_behaviortree
