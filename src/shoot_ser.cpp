/*******************************************************************************
 * Copyright (c) 2022/10/4, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <actionlib/server/simple_action_server.h>
#include <demo_behaviortree/ShootAction.h>  // Note: "Action" is appended

typedef actionlib::SimpleActionServer<demo_behaviortree::ShootAction> ShootServer;

void execute(const demo_behaviortree::ShootGoalConstPtr& goal, ShootServer* server) {
  demo_behaviortree::ShootFeedback feedback;
  demo_behaviortree::ShootResult result;
  //  ros::Rate r(2);  // 一秒开两枪
  bool success = true;

  for (int i = 0; i < goal->number_of_shots; i++) {
    if (server->isPreemptRequested() || !ros::ok()) {
      ROS_INFO("shoot is Preempted");
      success = false;
      break;
    }

    ROS_INFO("the NO.%d shoot ", i);
    feedback.which_shot = i;
    server->publishFeedback(feedback);
    ros::Duration(0.5).sleep();  // 一秒钟开一枪
                                 //    r.sleep();
  }

  if (success) {
    result.res = result.SUCCESS;
    server->setSucceeded(result);
  }
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "shoot_server");
  ros::NodeHandle n;
  ShootServer server(n, "shoot", boost::bind(&execute, _1, &server), false);
  server.start();
  ROS_INFO("Ready to shoot.");
  ros::spin();
  return 0;
}
