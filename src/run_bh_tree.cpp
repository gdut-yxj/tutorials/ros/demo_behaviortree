/*******************************************************************************
 * Copyright (c) 2022/10/04, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/
#include <behaviortree_cpp_v3/bt_factory.h>
#include <ros/ros.h>

#include "demo_behaviortree/blackboard_read.h"
#include "demo_behaviortree/calculate.h"
#include "demo_behaviortree/self_intro.h"
#include "demo_behaviortree/shoot.h"

int main(int argc, char** argv) {
  ros::init(argc, argv, "run_bh_tree");
  ros::NodeHandle root_nh;
  ros::NodeHandle bh_tree_nh("~");

  BT::BehaviorTreeFactory factory;

  //  1. 自我介绍,包含两个参数: my_name, age. 参数定义在行为树xml文件里面
  factory.registerNodeType<demo_behaviortree::SelfIntro>("SelfIntroduction");

  //  2. 读取行为树 blackboard里面数据的值
  //  note: MainTree 和 SubA 有各自的 blackboard， 这两个blackboard 想要相互读取对方的数据，需要 Remapping
  //  ref: https://www.behaviortree.dev/tutorial_06_subtree_ports/
  factory.registerNodeType<demo_behaviortree::BlackboardRead>("BlackboardRead");

  //  3. 调用 AddTwoInts 服务，计算两个int值相加的结果
  //  note: 使用了带参数(root_nh, bh_tree_nh)的注册方法，先创建 builder
  BT::NodeBuilder builder_calculate = [&root_nh, &bh_tree_nh](const std::string& name,
                                                              const BT::NodeConfiguration& config) {
    return std::make_unique<demo_behaviortree::Calculate>(name, config, root_nh, bh_tree_nh);
  };
  factory.registerBuilder<demo_behaviortree::Calculate>("AddTwoInts", builder_calculate);

  //  4. 调用 Shoot 服务，随机设置射击次数
  //  note: 在ros 的action 使用的是client/server模式，因此BT::SyncActionNode 可以很简单地实现异步和同步 action
  //  异步方式：在每次tick的时候使用action的client查看server 的状态和feedback，可以根据具体情况发送cancel 或者更改goal
  //  同步方式：直接在tick里面去阻塞等待action的完成即可。
  // ref: https://www.behaviortree.dev/asynchronous_nodes/
  BT::NodeBuilder builder_shoot = [&root_nh, &bh_tree_nh](const std::string& name,
                                                          const BT::NodeConfiguration& config) {
    return std::make_unique<demo_behaviortree::Shoot>(name, config, root_nh, bh_tree_nh);
  };
  factory.registerBuilder<demo_behaviortree::Shoot>("Shoot", builder_shoot);

  //  5. 读取行为树的xml文件, 根据上面具体行为的实现方法生成行为树
  //  ref: https://www.behaviortree.dev/tutorial_06_subtree_ports/
  //  ref: https://www.behaviortree.dev/tutorial_07_multiple_xml/
  std::string file_path = bh_tree_nh.param("file_path", std::string(" "));
  auto tree = factory.createTreeFromFile(file_path);

  ros::Rate loop_rate(5);  // 5HZ,设置行为树的运行频率
  while (ros::ok()) {
    BT::NodeStatus status = tree.tickRoot();
    std::cout << status << std::endl;
    loop_rate.sleep();
    ros::spinOnce();
  }
  return 0;
}